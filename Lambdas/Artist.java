package com.realdolmen;

import java.util.List;

public class Artist {
    private final String groupName;
    private final String origin;
    private final List<String> members;

    public Artist(String groupName, String origin, List<String> members) {
        this.groupName = groupName;
        this.origin = origin;
        this.members = members;
    }

    public String getGroupName() {
        return groupName;
    }

    public String getOrigin() {
        return origin;
    }

    public List<String> getMembers() {
        return members;
    }
}
