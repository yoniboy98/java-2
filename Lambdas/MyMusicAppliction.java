package com.realdolmen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MyMusicAppliction {
    public static void main(String[] args) {
        List<String> members = new ArrayList<>();
        List<MusicTrack> tracks = new ArrayList<>();
        members.add("John Lennon");
        Artist artist = new Artist("The beatles", "liverpool", members);
        tracks.addAll(Arrays.asList(new MusicTrack("Yellow submarine")
                , new MusicTrack("Yellow submarine2")
                , new MusicTrack("Yellow submarine3")
                , new MusicTrack("Yellow submarine4")));
        System.out.println(printArtist(Arrays.asList(artist)));
        System.out.println(printAlbums(tracks, members));
    }

    private static List<Album> printAlbums(List<MusicTrack> tracks, List<String> members) {
        Album newAlbum = new Album("Revolver", tracks, members);
        List<Album> albums = Arrays.asList(newAlbum, newAlbum, newAlbum);

        return albums.stream().map(album ->
                new Album(album.getAlbumName(), album.getTracks().stream().limit(3).collect(Collectors.toList()), album.getMembers())
        ).collect(Collectors.toList());
    }

    private static List<String> printArtist(List<Artist> artists) {
        return artists.stream().map(artist1 -> artist1.getMembers() + " origin:" + artist1.getOrigin()).collect(Collectors.toList());
    }
}
