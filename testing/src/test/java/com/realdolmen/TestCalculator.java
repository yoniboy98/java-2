package com.realdolmen;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)

public class TestCalculator {

    @Mock
    MathUtil mu;

    @InjectMocks
    Calculator c;

    @BeforeEach
    void setUp() {
  //      Mockito.when(mu.getSum(5, 10)).thenReturn(15);
    //    Mockito.when(mu.multiply(15, 10)).thenReturn(150);
    }

    @Test
    public void testSumAndMultiply() {
        int result = c.sumAndMultiply(0,0,0);
        Assertions.assertTrue(0 == result);
    }

    @Test
    public void testExecuteSumOnce(){
        int result = c.sumAndMultiply(5,5,10);
        Mockito.verify(mu, Mockito.times(1)).getSum(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    public void testExecuteMultipleOnce(){
        int result = c.sumAndMultiply(5, 5, 10);
        Mockito.verify(mu, Mockito.times(1)).getSum(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    public void testPersonHasCorrectName(){
        ArgumentCaptor<Person> argCap = ArgumentCaptor.forClass(Person.class);
        c.printPerson();
        Mockito.verify(mu).showPerson(argCap.capture());
        Assertions.assertEquals("Jimmi", argCap.getValue().getName());
    }
}

