package Generator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;


public class GeneratorTest {

     @Test
     public void testThrowsExceptionMaxSmallerThanMin() {
          assertThrows(IllegalArgumentException.class, () -> {
               RandomGenerator.numberMax(-10);
          });
     }

     @Test
     public void testThrowsExceptionMaxEqualsToMin() {
          assertThrows(IllegalArgumentException.class, () -> {
               RandomGenerator.numberMax(0);
          });
     }

     @Test
     public void testNumbersBetweenLimits() {
          int x = RandomGenerator.numberMax(5);

          assertTrue(x >= 0 && x <= 4);
     }

     @Test
     public void testFirstNameAppearsInArray() {
          String x = RandomGenerator.getRandomFirstName();
          String test = Arrays.stream(RandomGenerator.getFirstNames()).filter(n -> n.equals(x)).findFirst().get();
          assertEquals(x, test);
     }

     @Test
     public void testLastNameAppearsInArray() {
          String x = RandomGenerator.getRandomLastName();
          String test = Arrays.stream(RandomGenerator.getLastNames()).filter(n -> n.equals(x)).findFirst().get();
          assertEquals(x, test);
     }

     @Test
     public void testFirstNameIsNotNullOrEmpty() {
          String x = RandomGenerator.getRandomFirstName();
          assertFalse(x == null || x.isEmpty());
     }

     @Test
     public void testLastNameIsNotNullOrEmpty() {
          String x = RandomGenerator.getRandomLastName();
          assertFalse(x == null || x.isEmpty());
     }

     @Test
     public void testPasswordLengthIsGivenLength() {
          String password = RandomGenerator.password(5);
          assertEquals(5, password.length());
     }

     @Test
     public void testEmailIsNameLastNameSuffix() throws EmailMissingNameException {
          String email = RandomGenerator.email("yoni", "vindelinckx");
          assertEquals("yoni.vindelinckx@realdolmen.com", email);
     }

     @Test
     public void testEmptyFirstOrLastNameThrowsException() throws EmailMissingNameException {
          Assertions.assertThrows(EmailMissingNameException.class, () -> ); }
}























