package com.realdolmen;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)


public class PersonServiceTest {

    private Person person = new Person("John", "Doe", new Date(), new Address("kleinestraat", "5", new City(null, "9999")));

    @Mock
    JdbcPersonRepository jdbcPersonRepository;

    @InjectMocks
    PersonService personService;

    @Test
    public void testSavePerson() {
        ArgumentCaptor<Person> ArgumentMatcher = ArgumentCaptor.forClass(Person.class);
        person.setId(8);
        personService.savePerson(person);
        verify(jdbcPersonRepository).save(ArgumentMatcher.capture());
        verifyNoMoreInteractions(jdbcPersonRepository);
        Assert.assertEquals(8, ArgumentMatcher.getValue().getId().intValue());
    }

    @Test // gedrag
    public void personFindTest() {
        when(jdbcPersonRepository.find(anyInt())).thenReturn(person);
        personService.findPerson(5);
        verify(jdbcPersonRepository).find(anyInt());
        verifyNoMoreInteractions(jdbcPersonRepository);




    }

    @Test  //data
    void assertFindById() {
        ArgumentCaptor<Integer> argumentMatcher = ArgumentCaptor.forClass(Integer.class);
        when(jdbcPersonRepository.find(argumentMatcher.capture())).thenReturn(person);
        personService.findPerson(1);
        Assertions.assertEquals(1, argumentMatcher.getValue().intValue());

    }
}


